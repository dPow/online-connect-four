<table class="board">
<#list 0..5 as row> <!--List ends are inclusive-->
  <tr>
  <#list 0..6 as col>
    <td id="spot-${row}-${col}" class="${board.getSpot(col,row)}">
        
        <div id="${board.getSpot(col,row)}" class="circle">
             <!--onmouseover="mouseOverColor(this)"
             onmouseout="mouseOutColor(this)"-->
        </div>
        
    </td>
  </#list>
  </tr>
</#list>
</table>