package teamDBD.connectFour;

import java.util.Arrays;

public class Board {
    private static final int nColumns = 7;
    private static final int nRows = 6;
    public static int[][] grid = new int[nColumns][nRows];
    public static int turnPlayer = 1;
    
    public Board(){
        for (int col = 0; col < nColumns; col++){
            Arrays.fill(grid[col], 0);
        }
        //String print = toString();
        //System.out.println(print);
        //System.out.println(grid.length + "grid size");
    }
    
    public Board(String oldBoard){
        for (int col = 0; col < nColumns; col++){
            Arrays.fill(grid[col], 0);
        }
    	String[] rows = oldBoard.split(",");
        for (int row = 0; row < rows.length; row++){//Last row is just a \n
            for (int col = 0; col < rows[row].length(); col++){
                grid[col][row] = Integer.parseInt(
                        String.valueOf(rows[row].charAt(col))
                        );
            }
        }
    	
    	//String print = toString();
        //System.out.println(print);
    }
    
    
    public int doMove(int m){
    	if (isColFull(m)) {
            System.out.println("ERROR: Column " + m + " is full");
            return 0;
        } else {
            for (int i = 0; i < grid[m].length; i++) {
                if (grid[m][i] == 0) {
                    grid[m][i] = turnPlayer;
                    break;
                }
            }
            if (isWinner(m) == 0) {
                changeTurn();
                return 0;
            } else {
                return isWinner(m);
            }
        }
    }
    
    public boolean isColFull(int c){
    	if(grid[c][5] ==1 || grid[c][5] == 2 ){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    private void changeTurn(){
    	if(turnPlayer == 1){
    		turnPlayer = 2;
    	}else{
    		turnPlayer = 1;
    	}
    }
    /**
     * Return a human readable version of this Board.
     *
     * @param separator A character used to separate the four rows.
     * @return 4 string of 4 characters, joined by separator
     */
    @Override
    public String toString(){
        return toString(',');
    }
    public String toString(char separator) {
        int[][] printGrid = new int[nRows][nColumns];
        
        //convert grid[col][row] to grid[row][col]
        for (int r = 0; r < nRows; r++){
            for (int c = 0; c < nColumns; c++){
                printGrid[r][c] = grid[c][r];
            }
        }
        
        StringBuilder sb = new StringBuilder();
        for (int[] row : printGrid) {
            for (int i : row) {
                sb.append(i);
            }
            sb.append(separator);
        }
        return sb.toString();
    }
    
    /**
     * Returns an int representing which player the
     * piece at the given row-col index belongs to.
     * 
     * Presented in the way a computer prints the screen:
     * (0,0)    (1,0)   ...
     * (0,1)    (1,1)
     * .
     * .
     * .
     * 
     * @param row
     * @param col
     * @return 
     *          0 - no player's piece is there
     *          1 - player one's piece is there
     *          2 - player two's piece is there
     */
    
    public int getSpot(int col, int row){
        int returnCol = col;
        int returnRow = nRows - 1 - row;
        return grid[returnCol][returnRow];
    }
    
    public int getNumberOfRows(){
        return nRows;
    }
    
    public int getNumberOfColumns(){
        return nColumns;
    }
    
    
    private int isWinner(int m){
    	
    	//check columns
    	for(int i = 0; i < 7; i ++){
    		int count = 0;
    		for(int j = 0; j < 6; j++){
    			if(grid[i][j] == turnPlayer){
    				count+=1;
    				if(count == 4){
    					return turnPlayer;
    				}
    			}else{
    				count = 0;
    			}
    		}
    	}
    	
    	//check rows
    	for(int i = 0; i < 6; i++){
    		int count = 0;
    		for(int j = 0; j < 7; j++){
    			if(grid[j][i] == turnPlayer){
    				count+=1;
    				if(count == 4){
    					return turnPlayer;
    				}
    			}else{
    				count = 0;
    			}
    		}
    	}
    	
    	//check up-right diagonals
    	//1
    	if(grid[0][2] == turnPlayer && grid[1][3] == turnPlayer && grid[2][4] == turnPlayer && grid[3][5] == turnPlayer){
    		return turnPlayer;
    	}
    	//2
    	if(checkUpRight(5, 0, 1) != 0){
    		return turnPlayer;
    	}
    	//3
    	if(checkUpRight(6, 0, 0) != 0){
    		return turnPlayer;
    	}
    	
    	//4
    	if(checkUpRight(6, 1, 0) != 0){
    		return turnPlayer;
    	}
    	
    	//5
    	if(checkUpRight(5, 2, 0) != 0){
    		return turnPlayer;
    	}
    	
    	//6
    	if(grid[3][0] == turnPlayer && grid[4][1] == turnPlayer && grid[5][2] == turnPlayer && grid[6][3] == turnPlayer){
    		return turnPlayer;
    	}
    	
    	//check up-left diagonals
    	//1
    	if(grid[6][2] == turnPlayer && grid[5][3] == turnPlayer && grid[4][4] == turnPlayer && grid[3][5] == turnPlayer){
    		return turnPlayer;
    	}
    	
    	//2
    	if(checkUpLeft(5, 0, 1) !=0){
    		return turnPlayer;
    	}
    	
    	//3
    	if(checkUpLeft(6, 0, 0) !=0) {
    		return turnPlayer;
    	}
    	
    	//4
    	if(checkUpLeft(6, 1, 0) != 0){
    		return turnPlayer;
    	}
    	
    	//5
    	if(checkUpLeft(5, 2, 0) != 0){
    		return turnPlayer;
    	}
    	
    	//6
    	if(grid[3][0] == turnPlayer && grid[2][1] == turnPlayer && grid[1][2] == turnPlayer && grid[0][3] == turnPlayer){
    		return turnPlayer;
    	}
    	return 0;
    }
    
    private int checkUpRight(int checks, int x, int y){
        int count = 0;
    	for(int i = 0; i < checks; i++){ 
    		if(grid[i+x][i+y] == turnPlayer){
    			count +=1;
    			if(count == 4){
    				return turnPlayer;
    			}
    		}
    		else{
                count = 0;
            }
    	}
    	return 0;
    }
    
    private int checkUpLeft(int checks, int x, int y){
        int count = 0;
    	for(int i = 0; i < checks; i++){
    		if(grid[(6-x)-i][i+y] == turnPlayer){
    			count +=1;
    			if(count == 4){
    				return turnPlayer;
    			}
    		}
    		else{
                count = 0;
            }
    	}
    	return 0;
    }
}