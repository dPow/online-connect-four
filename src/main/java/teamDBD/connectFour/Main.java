package teamDBD.connectFour;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import freemarker.template.Configuration;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import spark.ExceptionHandler;
import spark.ModelAndView;
import spark.QueryParamsMap;
import spark.Request;
import spark.Response;
import spark.Spark;
import spark.TemplateViewRoute;
import spark.template.freemarker.FreeMarkerEngine;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;

public class Main {
    public static Board boardSpots = null;
  /**
   * The below if/else if statements don't actually do anything.
   * Since we aren't running from command line, nothing changes
   * in the options parser.
   * The only part that's needed is runSparkServer();
   * 
   * @param args
   */
  public static void main(String[] args) {
    OptionParser parser = new OptionParser();

    parser.accepts("generate");

    OptionSpec<String> solveSpec =
      parser.accepts("solve").withRequiredArg().ofType(String.class);


    OptionSet options = parser.parse(args);
    
    if (options.has("generate")) {
      System.out.print(new Board().toString());
    } else if (options.has(solveSpec)) {
      //Board provided = new Board(options.valueOf(solveSpec));
      //System.out.println(provided.play());
    } else {
      runSparkServer();
      // PROBLEM M1.  runSparkServer() is NOT an infinite loop. Your program will get to
      // this line.  So why doesn't it exit?
    }
  }

  /**
   * Sets the port, loads the static files for javascript/css,
   * and sets up the server so that you have a play url and a
   * results url.
   */
  private static void runSparkServer() {
    Spark.setPort(4230);
      
    // We need to serve some simple static files containing CSS and JavaScript.
    // This tells Spark where to look for urls of the form "/static/*".
    Spark.externalStaticFileLocation("src/main/resources/static");

    // Development is easier if we show exceptions in the browser.
    Spark.exception(Exception.class, new ExceptionPrinter());

    // We render our responses with the FreeMaker template system.
    FreeMarkerEngine freeMarker = createEngine();
    //webSocket("/play", WebSocketHandler.class);

    Spark.get("/play", new ResultsHandler(), freeMarker);
    Spark.post("/play", new ResultsHandler(), freeMarker);
  }
  
  /**
   * Makes a new board and sets the title, board, and input.
   */
  private static class StartBoard implements TemplateViewRoute {
    @Override
      public ModelAndView handle(Request req, Response res) {
          QueryParamsMap qm = req.queryMap();

          Board board;
          if (qm.value("board") == null) {
              System.out.println("null");
              board = new Board();
          } else {
              board = new Board(qm.value("board"));
          }
          
          Map<String, Object> variables
                  = ImmutableMap.of("title", "Connect Four",
                          "board", board);
          return new ModelAndView(variables, "play.ftl");
      }
  }
  
  /**
   * Makes the results screen using the same board from PlayHandler.
   */
  private static class ResultsHandler implements TemplateViewRoute {
    @Override
    public ModelAndView handle(Request req, Response res) {
        QueryParamsMap qm = req.queryMap();
        
        Board board;
        if (qm.value("board") == null) {
            board = new Board();
            if (boardSpots == null){
                boardSpots = new Board();
            }
        } else {
            String moveSelection = qm.value("moveSelection");
            int columnSelection = Integer.parseInt(moveSelection);
            if (columnSelection != 100) {
                int winner = boardSpots.doMove(columnSelection);
                board = new Board(boardSpots.toString());
                if (winner != 0) {
                    Map<String, Object> variables = ImmutableMap.of(
                            "title", "Connect Four",
                            "winner", winner);
                    return new ModelAndView(variables, "results.ftl");
                }
            }
            else {
                board = new Board(boardSpots.toString());
            }
        }
        
        Map<String, Object> variables = ImmutableMap.of(
                "title", "Connect Four",
                "board", board);

        return new ModelAndView(variables, "play.ftl");
    }

    private static final Splitter BREAKWORDS =
      Splitter.on(Pattern.compile("\\W+")).omitEmptyStrings();
  }
  /*
  @WebSocket
  public class WebSocketHandler {

      private String sender, msg;

      @OnWebSocketConnect
      public void onConnect(Session user) throws Exception {
          String username = "User" + Chat.nextUserNumber++;
          Chat.userUsernameMap.put(user, username);
          Chat.broadcastMessage(sender = "Server", msg = (username + " joined the chat"));
      }

      @OnWebSocketClose
      public void onClose(Session user, int statusCode, String reason) {
          String username = Chat.userUsernameMap.get(user);
          Chat.userUsernameMap.remove(user);
          Chat.broadcastMessage(sender = "Server", msg = (username + " left the chat"));
      }

      @OnWebSocketMessage
      public void onMessage(Session user, String message) {
          Chat.broadcastMessage(sender = Chat.userUsernameMap.get(user), msg = message);
      }

  }
*/
  // You need not worry about understanding what's below here.

  private static FreeMarkerEngine createEngine() {
    Configuration config = new Configuration();
    File templates = new File("src/main/resources/spark/template/freemarker");
    try {
      config.setDirectoryForTemplateLoading(templates);
    } catch (IOException ioe) {
      System.out.printf("ERROR: Unable use %s for template loading.\n",
                        templates);
      System.exit(1);
    }
    return new FreeMarkerEngine(config);
  }

  private static final int INTERNAL_SERVER_ERROR = 500;
  private static class ExceptionPrinter implements ExceptionHandler {
    @Override
    public void handle(Exception e, Request req, Response res) {
      res.status(INTERNAL_SERVER_ERROR);
      StringWriter stacktrace = new StringWriter();
      try (PrintWriter pw = new PrintWriter(stacktrace)) {
        pw.println("<pre>");
        e.printStackTrace(pw);
        pw.println("</pre>");
      }
      res.body(stacktrace.toString());
    }
  }

}
