package testpackage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import teamDBD.connectFour.Board;

/**
 *
 * @author dPow
 */
public class BoardTest {
    
    @Test
    public void testBoardBlankConstructor(){
        System.out.println("This is the blank constructor:\n");
        Board board = new Board();
        System.out.println(board);
    }
    
    @Test
    public void testBoardFromStringConstructor(){
        System.out.println("This is the blank constructor that "
                + "will make the non-blank constructor:\n");
        Board board = new Board();
        System.out.println(board);
        System.out.println("This is the non-blank constructor:\n");
        board.doMove(1);
        board.doMove(2);
        Board board2 = new Board(board.toString());
        System.out.println(board2);
    }
    
    @Test
    public void testDoMove(){
        System.out.println("This is the move tester:\n");
        Board board = new Board();
        board.doMove(2);
        System.out.println(board.toString());
        board.doMove(1);
        board.doMove(2);
        System.out.println(board.toString());
    }
    
}
