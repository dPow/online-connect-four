var JavaBoard = document.getElementById("JavaBoard");

/*Sets the moveSelection input to the selected column*/
function jsDoMove(elem){
    var columnChoice = elem.id;
    document.getElementById("move").value = columnChoice;
    var forms = document.getElementsByTagName("form");
    forms[0].submit();
}

function updateCircles(circles){
    var i;
    for (i = 0; i < circles.length; i++){
        var circleElement = circles[i];
        var circleIndex = circleElement.id;
        
        if (circleIndex == 0) {
            circleElement.style.background = "rgba(100,100,250,0.3)";
        } else if (circleIndex == 1) {
            circleElement.style.background = "red";
        } else if (circleIndex == 2) {
            circleElement.style.background = "blue";
        }
    }
}

function updateScreen(){
    location.reload(true);
    /*setTimeout(updateScreen(), 1000);*/
}