<#assign content>


<#include "board.ftl">

<!--Update circle colors after the board has been made/loaded-->
<script>updateCircles(document.getElementsByClassName("circle"))</script>


<form method="POST" action="/play">

    <table id="submitButtons">
        <tr>
            <td>
                <button id="0" onclick="jsDoMove(this)">Move Here</button>
            </td>
            <td>
                <button id="1" onclick="jsDoMove(this)">Move Here</button>
            </td>
            <td>
                <button id="2" onclick="jsDoMove(this)">Move Here</button>
            </td>
            <td>
                <button id="3" onclick="jsDoMove(this)">Move Here</button>
            </td>
            <td>
                <button id="4" onclick="jsDoMove(this)">Move Here</button>
            </td>
            <td>
                <button id="5" onclick="jsDoMove(this)">Move Here</button>
            </td>
            <td>
                <button id="6" onclick="jsDoMove(this)">Move Here</button>
            </td>
        </tr>
    </table>

    <button type="button" id="100" onclick="jsDoMove(this)">Refresh screen</button>
    <input type="hidden" id="move" name="moveSelection">
    <!--
    <textarea id="id_guesses" name="guesses" placeholder="Enter words here"></textarea>
    <input type="submit">
    -->
    <input type="hidden" name="board" value="${board.toString(',')}">
</form>

</#assign>
<#include "main.ftl">


